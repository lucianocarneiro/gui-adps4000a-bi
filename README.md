# PicoScope 4000a for BI

OPIs for for PicoScope. This project was based on [areadetector-gui](https://gitlab.esss.lu.se/icshwi/nss-instruments/gui-areadetector).

The Macros configured are:

| variable | value    |
|----------|:--------:|
|P|  TS2-Row010:|
|R (1st device)|  Scope-001:|
|R (2nd device)|  Scope-002:|


## To run
Execute `launcher.bob` using Phoebus.
